package com.pinkycindy.petrograt.other;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Fikkri Prasetya on 9/26/2017.
 */

public class LoginRequest extends StringRequest {
    private Map<String, String> params;

    public LoginRequest(String nik, String password, String url, Response.Listener<String> listener) {
        super(Method.POST, url, listener, null);
        params = new HashMap<>();
        params.put("nik", nik);
        params.put("password", password);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}