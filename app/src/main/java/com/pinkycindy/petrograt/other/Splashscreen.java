package com.pinkycindy.petrograt.other;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.pinkycindy.petrograt.R;
import com.pinkycindy.petrograt.activity.MainActivity;
import com.pinkycindy.petrograt.activity.WelcomeActivity;

//created by devira

public class Splashscreen extends AppCompatActivity {

    SessionManager session;

    // mengatur waktu lamanya splash screen
    private static int splashInterval = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.splashscreen);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                session = new SessionManager(Splashscreen.this);
                if(session.isLoggedIn()) {
                    startActivity(new Intent(Splashscreen.this, MainActivity.class));
                    finish();
                }else{
                    startActivity(new Intent(Splashscreen.this,  WelcomeActivity.class));
                    finish();
                }
            }

            private void finish() {
                //TODO Auto-generated method stub
            }
        }, splashInterval);
    };
}
