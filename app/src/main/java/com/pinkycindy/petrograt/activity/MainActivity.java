package com.pinkycindy.petrograt.activity;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.pinkycindy.petrograt.R;
import com.pinkycindy.petrograt.fragment.FaqFragment;
import com.pinkycindy.petrograt.fragment.HistoryFragment;
import com.pinkycindy.petrograt.fragment.HomeFragment;
import com.pinkycindy.petrograt.fragment.ProfilFragment;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

public class MainActivity extends AppCompatActivity {
    private VPMainAdapter fragmentPagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Menerapkan TabLayout dan ViewPager pada Activity
        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tl_main);
        final CustomViewPager cViewPager = (CustomViewPager) findViewById(R.id.vp_main);
        cViewPager.setPagingEnabled(false);

        //Memanggil dan Memasukan Value pada Class PagerAdapter(FragmentManager dan JumlahTab)
        fragmentPagerAdapter = new VPMainAdapter(getSupportFragmentManager(), tabLayout.getTabCount());

        KeyboardVisibilityEvent.setEventListener(this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        tabLayout.setVisibility(isOpen ? View.GONE : View.VISIBLE);
                    }
                });

        //Memasang Adapter pada ViewPager
        cViewPager.setAdapter(fragmentPagerAdapter);
        /*
         Menambahkan Listener yang akan dipanggil kapan pun halaman berubah atau
         bergulir secara bertahap, sehingga posisi tab tetap singkron
         */
        cViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        //Callback Interface dipanggil saat status pilihan tab berubah.
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //Dipanggil ketika tab memasuki state/keadaan yang dipilih.
                cViewPager.setCurrentItem(tab.getPosition());
                tab.getIcon().setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_IN);
                //Dipanggil saat tab keluar dari keadaan yang dipilih.
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN);
                //Dipanggil ketika tab yang sudah dipilih, dipilih lagi oleh user.
            }
        });


    }

    private class VPMainAdapter extends FragmentPagerAdapter {

        private int number_tabs;

        public VPMainAdapter(FragmentManager fm, int number_tabs) {
            super(fm);
            this.number_tabs = number_tabs;
        }

        //Mengembalikan Fragment yang terkait dengan posisi tertentu
        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return new HomeFragment();
                case 1:
                    return new HistoryFragment();
                case 2:
                    return new FaqFragment();
                case 3:
                    return new ProfilFragment();
                default:
                    return null;
            }
        }

        //Mengembalikan jumlah tampilan yang tersedia.
        @Override
        public int getCount() {
            return number_tabs;
        }
    }


}
