package com.pinkycindy.petrograt.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.pinkycindy.petrograt.R;
import com.pinkycindy.petrograt.other.LoginRequest;
import com.pinkycindy.petrograt.other.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Pinky Cindy on 02/26/18.
 */

public class LoginActivity  extends AppCompatActivity{

    Button btnLogin;
    EditText tNik, tPass;
    SessionManager session;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tNik = (EditText) findViewById(R.id.input_nik);
        tPass = (EditText) findViewById(R.id.input_password);
        btnLogin = (Button) findViewById(R.id.button_login);

        session = new SessionManager(getApplicationContext());

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String nik = tNik.getText().toString();
                final String pass = tPass.getText().toString();

                if (nik.length() == 0) {
                    //jika form Username belum di isi / masih kosong
                    tNik.setError("Please Enter your Username!");
                    tNik.requestFocus();
                } else if (pass.length() == 0) {
                    //jika form Passwrod belum di isi / masih kosong
                    tPass.requestFocus();
                    Toast.makeText(LoginActivity.this, "Please Enter your Password!", Toast.LENGTH_SHORT).show();
                } else {
                    progressDialog = new ProgressDialog(LoginActivity.this);
                    progressDialog.setTitle("Please Wait");
                    progressDialog.setMessage("Processing...");
                    progressDialog.show();

                    Response.Listener<String> responseListener = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try{
                                JSONObject jsonResponse = new JSONObject(response);
                                boolean succes = jsonResponse.getBoolean("success");

                                if(succes){
                                    progressDialog.dismiss();
                                    String idUser = jsonResponse.getString("id_user");
                                    String nama = jsonResponse.getString("nama");
                                    String email = jsonResponse.getString("email");
                                    String username = tNik.getText().toString();
                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    intent.putExtra("idUser", idUser);
                                    intent.putExtra("nama", nama);
                                    intent.putExtra("email", email);
                                    intent.putExtra("username", username);
                                    LoginActivity.this.startActivity(intent);
                                    session.createLoginSession(idUser, nama, username, email);
                                    finish();
                                }
                                else {
                                    progressDialog.dismiss();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                    builder.setMessage("Login Failed")
                                            .setNegativeButton("Retry", null)
                                            .create()
                                            .show();
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    };
                    LoginRequest loginRequest = new LoginRequest(nik, pass, getString(R.string.api)+"Login.php", responseListener);
                    RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
                    queue.add(loginRequest);

                }
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }
}
