package com.pinkycindy.petrograt.fragment;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.pinkycindy.petrograt.R;
import com.pinkycindy.petrograt.other.SessionManager;

import java.util.HashMap;

/**
 * Created by Pinky Cindy on 02/23/18.
 */

public class ProfilFragment extends Fragment {

    Button btnLogout;
    SessionManager session;
    TextView nama;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profil, container, false);

        btnLogout = (Button) view.findViewById(R.id.btn_logout);
        nama = (TextView) view.findViewById(R.id.tNama);

        session = new SessionManager(getActivity());
        session.checkLogin();
        HashMap<String, String> user = session.getUserDetails();

        String namax = user.get(SessionManager.KEY_NAMA);

        nama .setText(namax);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Logout")
                        .setMessage("Ingin Keluar Dari Aplikasi Ini?")
                        .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Toast.makeText(getActivity(), "Kamu Memilih YES", Toast.LENGTH_LONG).show();
                                dialog.cancel();
                                session.logoutUser();
                                getActivity().finish();
                            }
                        })
                        .setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Toast.makeText(getActivity(), "Kamu Memilih TIDAK Ingin Keluar", Toast.LENGTH_LONG).show();
                                dialog.cancel();
                            }
                        }).show();
            }
        });
        return view;
    }
}
